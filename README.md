# OpenML dataset: wholesale-customers

https://www.openml.org/d/1511

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Margarida G. M. S. Cardoso      
**Source**: UCI     
**Please cite**: Abreu, N. (2011). Analise do perfil do cliente Recheio e desenvolvimento de um sistema promocional. Mestrado em Marketing, ISCTE-IUL, Lisbon.  

* Title:   
Wholesale customers Data Set 

* Abstract:   
The data set refers to clients of a wholesale distributor. It includes the annual spending in monetary units (m.u.) on diverse product categories

* Source:  
Margarida G. M. S. Cardoso, margarida.cardoso '@' iscte.pt, ISCTE-IUL, Lisbon, Portugal

* Attribute Information:

1) FRESH: annual spending (m.u.) on fresh products (Continuous); 
2) MILK: annual spending (m.u.) on milk products (Continuous); 
3) GROCERY: annual spending (m.u.)on grocery products (Continuous); 
4) FROZEN: annual spending (m.u.)on frozen products (Continuous) 
5) DETERGENTS_PAPER: annual spending (m.u.) on detergents and paper products (Continuous) 
6) DELICATESSEN: annual spending (m.u.)on and delicatessen products (Continuous); 
7) CHANNEL: customers' Channel - Horeca (Hotel/Restaurant/Café) or Retail channel (Nominal) 
8) REGION: customers' Region - Lisbon, Porto or Other (Nominal) 

Descriptive Statistics: 

(Minimum, Maximum, Mean, Std. Deviation) 
FRESH ( 3, 112151, 12000.30, 12647.329) 
MILK (55, 73498, 5796.27, 7380.377) 
GROCERY (3, 92780, 7951.28, 9503.163) 
FROZEN (25, 60869, 3071.93, 4854.673) 
DETERGENTS_PAPER (3, 40827, 2881.49, 4767.854) 
DELICATESSEN (3, 47943, 1524.87, 2820.106) 

REGION Frequency 
Lisbon 77 
Oporto 47 
Other Region 316 
Total 440 

CHANNEL Frequency 
Horeca 298 
Retail 142 
Total 440 


* Relevant Papers:

Cardoso, Margarida G.M.S. (2013). Logical discriminant models â€“ Chapter 8 in Quantitative Modeling in Marketing and Management Edited by Luiz Moutinho and Kun-Huang Huarng. World Scientific. p. 223-253. ISBN 978-9814407717 

Jean-Patrick Baudry, Margarida Cardoso, Gilles Celeux, Maria JosÃ© Amorim, Ana Sousa Ferreira (2012). Enhancing the selection of a model-based clustering with external qualitative variables. RESEARCH REPORT NÂ° 8124, October 2012, Project-Team SELECT. INRIA Saclay - ÃŽle-de-France, Projet select, UniversitÃ© Paris-Sud 11

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1511) of an [OpenML dataset](https://www.openml.org/d/1511). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1511/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1511/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1511/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

